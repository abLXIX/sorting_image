// Frame_sorting.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define DUMP_YUV 0

constexpr unsigned char g_LUMA_UPPER_THRESHOLD = 210;
constexpr unsigned char g_LUMA_LOWER_THRESHOLD = 50;

static unsigned char* sort_frame_horizontally(const unsigned char* data, const int width, const int height, const int no_of_component);
static unsigned char* sort_frame_vertically(const unsigned char* data, const int width, const int height, const int no_of_component);
static unsigned char* sort_contrast_frame_horizontally(const unsigned char* data, const int width, const int height, const int no_of_component);
static unsigned char* sort_contrast_frame_vertically(const unsigned char* data, const int width, const int height, const int no_of_component);
static unsigned char* get_contrast_mask_image(const unsigned char* data, const int width, const int height, const int no_of_component);

enum class SORTING_TYPE
{
    HORIZONTAL_SORT,
    VERTICAL_SORT,
    CONTRAST_HORIZONTAL_SORT,
    CONTRAST_VERTICAL_SORT,
    CONTRAST_MASK,
};
int 
main(int argc, char* argv[])
{
    /*
     *  NOTE:   It's assumed that the input image file will always be RGB 8-bit.
     *          Image format support depends on stb_image.h .
     *          Output file extension must always be .png
     */
    if (argc != 4)
    {
        std::cout << "Incorrect usage.\nCorrect usage: program_name.exe sort_type input_file_path output_file_path\n";
        return -1;
    }
    int width;
    int height;
    int no_of_components;
    unsigned char* image_data = stbi_load(argv[2],
                                          &width, 
                                          &height, 
                                          &no_of_components, 
                                          0);

    if (no_of_components != 3)
    {
        std::cout << "ERRR! The number of components in the image is not 3.\n";
        if (image_data)
            STBI_FREE(image_data);

        return -1;
    }
    if (image_data)
    {
        
        unsigned char* sorted_frame = nullptr;
        
        SORTING_TYPE sorting_type = (SORTING_TYPE)atoi(argv[1]);
        switch (sorting_type)
        {
        case SORTING_TYPE::HORIZONTAL_SORT:
            sorted_frame = sort_frame_horizontally(image_data, width, height, no_of_components);
            break;

        case SORTING_TYPE::VERTICAL_SORT:
            sorted_frame = sort_frame_vertically(image_data, width, height, no_of_components);
            break;

        case SORTING_TYPE::CONTRAST_HORIZONTAL_SORT:
            sorted_frame = sort_contrast_frame_horizontally(image_data, width, height, no_of_components);
            break;

        case SORTING_TYPE::CONTRAST_VERTICAL_SORT:
            sorted_frame = sort_contrast_frame_vertically(image_data, width, height, no_of_components);
            break;

        case SORTING_TYPE::CONTRAST_MASK:
            sorted_frame = get_contrast_mask_image(image_data, width, height, no_of_components);
            break;

        default:
            std::cout << "WRONG SORTING TYPE!!!! Try again.\n";
        }

        if (sorted_frame)
        {
            stbi_write_png(argv[3], width, height, 3, sorted_frame, width * 3);
            delete[] sorted_frame;
            sorted_frame = nullptr;
        }

        STBI_FREE(image_data);
    }

    return 0;
}

static inline void 
sort(unsigned char* row_data, unsigned long row_size)
{
    unsigned char temp[3];
    for (int i = 0; i < row_size; i += 3)
    {
        for (int j = 0; j < row_size - 3; j += 3)
        {
            if (row_data[j] > row_data[j + 3])
            {
                memcpy(temp, &row_data[j], 3);
                memcpy(&row_data[j], &row_data[j + 3], 3);
                memcpy(&row_data[j + 3], temp, 3);
            }
        }
    }
}

static inline void 
convert_RGB_to_YUV(unsigned char* row_data, const int row_size)
{
	unsigned char Y;
	unsigned char U;
	unsigned char V;

	unsigned char R;
	unsigned char G;
	unsigned char B;

	for (int i = 0; i < row_size; i += 3)
	{
		R = row_data[i];
		G = row_data[i + 1];
		B = row_data[i + 2];

        // make sure to clip the output to [0, 255] working with 8-bit. Had a major headache because I forgot.
        int iY = R * .299000 + G * .587000 + B * .114000;
        int iU = R * -.168736 + G * -.331264 + B * .500000 + 128;
        int iV = R * .500000 + G * -.418688 + B * -.081312 + 128;

        Y = (unsigned char)(iY < 0 ? 0 : (iY > 255 ? 255 : iY));
        U = (unsigned char)(iU < 0 ? 0 : (iU > 255 ? 255 : iU));
        V = (unsigned char)(iV < 0 ? 0 : (iV > 255 ? 255 : iV));

        //// Not working
        //Y = R * 0.299 + G * 0.587 + B * 0.114;
        //U = R * -0.14713 + G * -0.28886 + B * 0.436;
        //V = R * 0.615 + G * -0.51499 + B * -0.10001;
        
        ////Not working
        //Y = (R * 0.2126) + (G * 0.7152) + (B * 0.0722);
        //U = (R * -0.09991) + (G * -0.33609) + (B * 0.436);
        //V = (R * 0.615) + (G * -0.55861) + (B * -0.05639);

        //// There are still some artifacts
        //Y = 0.299 * R + 0.587 * G + 0.114 * B;
        //U = -0.14713* R - 0.28886 * G + 0.436 * B + 128;
        //V = 0.615 * R - 0.51499 * G - 0.10001 * B + 128;

		row_data[i]     = Y;
		row_data[i + 1] = U;
		row_data[i + 2] = V;
	}
}

static inline void 
convert_YUV_to_RGB(unsigned char* row_data, const int row_size)
{
	unsigned char Y;
	unsigned char U;
	unsigned char V;

	unsigned char R;
	unsigned char G;
	unsigned char B;

	for (int i = 0; i < row_size; i += 3)
	{
		Y = row_data[i];
		U = row_data[i + 1];
		V = row_data[i + 2];

        // make sure to clip the output to [0, 255] working with 8-bit. Had a major headache because I forgot.
        int iR = (int)(Y + 1.4075 * (V - 128));
        int iG = (int)(Y - 0.3455 * (U - 128) - (0.7169 * (V - 128)));
        int iB = (int)(Y + 1.7790 * (U - 128));

        R = (unsigned char)(iR < 0 ? 0 : (iR > 255 ? 255 : iR));
		G = (unsigned char)(iG < 0 ? 0 : (iG > 255 ? 255 : iG));
		B = (unsigned char)(iB < 0 ? 0 : (iB > 255 ? 255 : iB));

        ////Not working
        //R = Y + (V * 1.13983);
        //G = Y + (U * -0.39465) + (V * -0.58060);
        //B = Y + (U * 2.03211);

        ////Not working
        //R = Y + (V * 1.28033);
        //G = Y + (U * -0.21482) + (V * -0.38059);
        //B = Y + (U * 2.12798);

        //// there are still some artifacts
        //R = Y + 1.403 * (V - 128);
        //G = Y - 0.344 * (U - 128) - 0.714 * (V - 128);
        //B = Y + 1.770 * (U - 128);

		row_data[i]     = R;
		row_data[i + 1] = G;
		row_data[i + 2] = B;
	}
}

unsigned char* 
sort_frame_horizontally(const unsigned char* data, const int width, const int height, const int no_of_component)
{
    unsigned char*      return_buffer = nullptr;
    const unsigned long row_size      = width * no_of_component;

    unsigned char*      row_data      = new unsigned char[row_size];
    unsigned char*      sorted_data   = new unsigned char[width * height * no_of_component];

    if (!sorted_data)
    {
        goto FAILURE;
    }
    if (!row_data)
    {
        delete[] sorted_data;
        goto FAILURE;
    }

    for (int j = 0; j < height; ++j)
    {
        std::clog << "\rRows remaining: " << (height - j + 1) << " " << std::flush;
        memcpy(row_data, &data[row_size * j], row_size);
        
        sort(row_data, row_size);

        memcpy(&sorted_data[row_size * j], row_data, row_size);
    }
    return_buffer = sorted_data;


FAILURE:
    if (row_data)    delete[] row_data;

    return return_buffer;
}

unsigned char*
sort_frame_vertically(const unsigned char* data, const int width, const int height, const int no_of_component)
{
    unsigned char*      return_buffer    = nullptr;
    const unsigned long frame_size       = width * height * no_of_component;
    const int           row_size         = width * no_of_component;
                                         
    unsigned char* sorted_data           = new unsigned char[frame_size];
    memcpy(sorted_data, data, frame_size);

    if (!sorted_data)
    {
        return nullptr;
    }
    unsigned char temp[3]  = { 0 };

    for (int j = 0; j < row_size; j += 3)
    {
        std::clog << "\rColumns remaining: " << (width * no_of_component - j + 1) << " " << std::flush;

        for (int i = 0; i < width * height * no_of_component; i += row_size)
        {
            for (int k = 0; k < width * (height - 1) * no_of_component; k += row_size)
            {
                int top = sorted_data[k + j];
                int bottom = sorted_data[k + row_size + j];
                if (top > bottom)
                {
                    memcpy(temp, &sorted_data[k + j], 3);
                    memcpy(&sorted_data[k + j], &sorted_data[k + row_size + j], 3);
                    memcpy(&sorted_data[k + row_size + j], temp, 3);
                }
            }
        }
    }
    
    return sorted_data;
}

static unsigned char* 
get_contrast_mask(const unsigned char* yuv_data, const unsigned long frame_size)
{
    unsigned char* Y_mask_data = new unsigned char[frame_size];
    memcpy(Y_mask_data, yuv_data, frame_size);
    for (int i = 0; i < frame_size; i += 3)
    {
        unsigned char Y_data = Y_mask_data[i];
        /*if (Y_data > g_LUMA_UPPER_THRESHOLD)
            yuv_data[i] = 255;
        else if (Y_data < g_LUMA_LOWER_THRESHOLD)
            yuv_data[i] = 0;*/

        if (Y_data < g_LUMA_LOWER_THRESHOLD || Y_data > g_LUMA_UPPER_THRESHOLD)
            Y_mask_data[i] = 0;
        else
            Y_mask_data[i] = 255;
    }

    return Y_mask_data;
}

static void
sort_common_luminance(unsigned char*       RGB_data, 
                      const unsigned char* Y_contrast_mask, 
                      const int            width, 
                      const int            height, 
                      const int            no_of_component,
                      const bool           sort_only_white_luma)
{
    const unsigned long frame_size = width * height * no_of_component;
    const unsigned int  row_size   = width * no_of_component;

    unsigned char* row_data = new unsigned char[row_size];
    for (int j = 0; j < height; ++j)
    {
        std::clog << "\rRows remaining: " << (height - j + 1) << " " << std::flush;
        memcpy(row_data, RGB_data + (j * row_size), row_size);

        // find equal luminance and sort them
        unsigned char Y = Y_contrast_mask[0 + (j * row_size)];
        unsigned int count = 1;
        unsigned int index = 0;
        bool do_sorting = true;
        for (int i = 3; i < row_size; i += 3)
        {
            if (Y == Y_contrast_mask[i + 3 + (j * row_size)])
            {
                count++;
                continue;
            }
            // sort the data and get the new Y component
            if (sort_only_white_luma && Y != 255)
                do_sorting = false;
            else if (sort_only_white_luma && Y == 255)
                do_sorting = true;

            if (count > 1 && do_sorting)
            {
                // we take the row which has common Y values, and then use the it's starting point and range 
                // to sort the data in the RGB buffer in the same place with R value as the comparison object
                // so find the common Y, take its range and start position and sort its equivalent RGB buffer range, from postion of start.
                sort(row_data + index, count * 3);
            }

            Y = Y_contrast_mask[i + 3 + (j * row_size)];
            count = 1;
            index = i;
        }

        if (sort_only_white_luma && Y != 255)
            do_sorting = false;
        else if (sort_only_white_luma && Y == 255)
            do_sorting = true;

        if (count > 1 && do_sorting)
        {
            sort(row_data + index, (count * 3));
        }

        memcpy(RGB_data + (j * row_size), row_data, row_size);
    }

    delete[] row_data;
  
}

/*
 *  Steps:
 *  1) convert RGB to YUV.
 * 
 *  2) generate contrast mask from YUV buffer, and store it. Any Y value above max luma and below min luma will be replaced by 0, and the rest will be 
 *      replaced with 255 Y value.
 * 
 *  3) convert YUV buffer back to RGB. We want to do the sorting in this buffer, and the resulting image also has to be in RGB.
 * 
 *  4) pass both RGB and contrast mask buffer for sorting. Contrast mask will have all the Y values as either 255 or 0.
 *      whenever we find continuous segment of Y value of 255, we will take it's position and range, and sort the equivalent RGB row from
 *      that position till the end of the range on the basis of R components value.(Personally I think it's better to sort on basis of the
 *      original Y component. Using either RGB or orginal YUV buffer won't change the final buffer that much. I have tested that.)
 */
unsigned char* 
sort_contrast_frame_horizontally(const unsigned char* data, const int width, const int height, const int no_of_component)
{
    const unsigned long frame_size = width * height * no_of_component;

    unsigned char* sorted_data = new unsigned char[frame_size];
    memcpy(sorted_data, data, frame_size);

    convert_RGB_to_YUV(sorted_data, frame_size);

#if DUMP_YUV
    // use C api(fopen) to keep program cross platform.
    int fd = _wopen(L"C:\\Users\\ninja\\source\\repos\\Frame_sorting\\x64\\Debug\\raw_YUV.yuv", _O_BINARY | _O_CREAT | O_WRONLY, S_IWRITE | S_IREAD);
    if (fd != -1)
    {
        /*unsigned char* new_sorted_data = new unsigned char[frame_size];
        unsigned char* Y = new_sorted_data;
        unsigned char* U = Y + (width * height);
        unsigned char* V = U + (width * height);
        for (int i = 0, k = 0; i < frame_size; i += 3, ++k)
        {
            Y[k] = sorted_data[i];
            U[k] = sorted_data[i + 1];
            V[k] = sorted_data[i + 2];
        }
        _write(fd, Y, frame_size);

        delete[] new_sorted_data;*/

        _write(fd, sorted_data, frame_size);
        _close(fd);
    }
#endif

    unsigned char* Y_contrast_mask = get_contrast_mask(sorted_data, frame_size);
    //memcpy(sorted_data, Y_contrast_mask, frame_size);
    convert_YUV_to_RGB(sorted_data, frame_size);

    sort_common_luminance(sorted_data, Y_contrast_mask, width, height, no_of_component, true);

    delete[] Y_contrast_mask;

    return sorted_data;
}

void sort_vertically(unsigned char*     RGB_data, 
                     unsigned int       count, 
                     const unsigned int step)
{
    unsigned char temp[3] = {0};

    for (int i = 0; i < count * step; i += step)
    {
        for (int j = 0; j < (count - 1) * step; j += step)
        {
            if (RGB_data[j] > RGB_data[j + step])
            {
                memcpy(temp, &RGB_data[j], 3);
                memcpy(&RGB_data[j], &RGB_data[j + step], 3);
                memcpy(&RGB_data[j + step], &RGB_data[j], 3);
            }
        }
    }
}

static void
sort_common_luminance_vertically(unsigned char*       RGB_data,
                                 const unsigned char* Y_contrast_mask,
                                 const int            width,
                                 const int            height,
                                 const int            no_of_component,
                                 const bool           sort_only_white_luma)
{
    const unsigned long frame_size = width * height * no_of_component;
    const unsigned long row_size = width * no_of_component;

    for (int i = 0; i < row_size; i += 3)
    {
        std::clog << "\rColumns remaining: " << (row_size - i + 1) << " " << std::flush;

        unsigned char Y = Y_contrast_mask[i];
        unsigned int index = i;
        unsigned int count = 1;
        bool do_sorting = true;
        for (unsigned int j = i + row_size; j < frame_size; j += row_size)
        {
            if (Y == Y_contrast_mask[j])
            {
                count++;
                continue;
            }

            if (sort_only_white_luma && Y != 255)
                do_sorting = false;
            else if (sort_only_white_luma && Y == 255)
                do_sorting = true;

            if (count > 1 && do_sorting)
            {
                sort_vertically(RGB_data + index, count, row_size);
            }

            Y     = Y_contrast_mask[j];
            count = 1;
            index = j;
        }

        if (sort_only_white_luma && Y != 255)
            do_sorting = false;
        else if (sort_only_white_luma && Y == 255)
            do_sorting = true;
        if (count > 1 && do_sorting)
        {
            sort_vertically(RGB_data + index, count, row_size);
        }
    }
}

/*
 *  Steps:
 *  1) All the conversion logic will be the same as that of sort_contrast_frame_horizontally().
 *  2) We will just sort in terms of colums for the component R.
 */
unsigned char* 
sort_contrast_frame_vertically(const unsigned char* data, const int width, const int height, const int no_of_component)
{
    const unsigned long frame_size = width * height * no_of_component;

    unsigned char* sorted_data = new unsigned char[frame_size];
    memcpy(sorted_data, data, frame_size);

    convert_RGB_to_YUV(sorted_data, frame_size);
    
    unsigned char* Y_contrast_mask = get_contrast_mask(sorted_data, frame_size);

    convert_YUV_to_RGB(sorted_data, frame_size);

    sort_common_luminance_vertically(sorted_data, Y_contrast_mask, width, height, no_of_component, true);

    delete[] Y_contrast_mask;

    return sorted_data;
}

unsigned char*
get_contrast_mask_image(const unsigned char* data, const int width, const int height, const int no_of_component)
{
    const unsigned long frame_size = width * height * no_of_component;

    unsigned char* frame_data = new unsigned char[frame_size];
    memcpy(frame_data, data, frame_size);

    convert_RGB_to_YUV(frame_data, frame_size);

    unsigned char* Y_contrast_mask = get_contrast_mask(frame_data, frame_size);

    convert_YUV_to_RGB(Y_contrast_mask, frame_size);

    delete[] frame_data;

    return Y_contrast_mask;
}
