Inspired by: https://www.youtube.com/watch?v=HMmmBDRy-jE&t=326s

Running the program: 
+ Frame_sorting.exe <sorting_type> <input_image_file_path> <output_png_path>

Sorting type:
- 0 --> sort horizontally
- 1 --> sort vertically
- 2 --> apply a contrast map, and then sort horizontally
- 3 --> apply a contrast map, and then sort vertically
- 4 --> apply contrast map only


TODO:
1. Use optimized sorting algorithm. The program take a lot of time to do sorting because the current implementation uses bubble sort.
2. Vertical sorting after contrast map application.
3. Use thread to speed up the process. Rows are independent of each other, so operations, sorting, etc can be done on them without any problem.



Best Results:
1. For horizontal sorting with contrast map:

*original.jpg*
<img title="original" src="/results/horizontal_sorting_with_contrast_map/original.jpg">

*after_sorting.png*
<img title="sorted" src="/results/horizontal_sorting_with_contrast_map/after_sorting.png">

2. For vertical sorting with contrast map:

*original.jpg*
<img title="original" src="/results/vertical_sorting_with_contrast_map/original.jpg">

*after_sorting.png*
<img title="sorted" src="/results/vertical_sorting_with_contrast_map/after_sorting.png">