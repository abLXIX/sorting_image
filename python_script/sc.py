import os
import sys
import subprocess

def run_sorting_of_frame(sorting_type, input_dir, output_dir):
    input_file_paths = []
    output_file_paths = []
    for dir_path, dir_names, filenames in os.walk(input_dir):
        for file in filenames:
                full_file_path = os.path.join(dir_path, file)
                input_file_paths.append(full_file_path)
                
                # output file will have the same name
                file_part = file.split('.')
                output_file_name = file_part[0] + '.png'
                output_file_path = os.path.join(output_dir, output_file_name)
                output_file_paths.append(output_file_path)
        break
        
    for i in range(len(output_file_paths)):
        print("\nFor Input file path", input_file_paths[i], ", and output file path", output_file_paths[i])
        cmd_args = f"Frame_sorting {sorting_type} {input_file_paths[i]} {output_file_paths[i]}" 
        #subprocess.run(["Frame_sorting.exe", cmd_args]) 
        os.system(cmd_args)
    
if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Incorrect usage")
    else:
        run_sorting_of_frame(sys.argv[1], sys.argv[2], sys.argv[3])